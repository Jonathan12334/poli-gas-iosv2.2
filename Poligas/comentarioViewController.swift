//
//  comentarioViewController.swift
//  Poligas
//
//  Created by ALEX VILATUÑA on 2/11/20.
//  Copyright © 2020 Quantum. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseStorage
import FirebaseCore
import FirebaseAuth

class comentarioViewController: UIViewController {
    let db = Firestore.firestore()
    
    
    @IBOutlet weak var txtcometario:
    UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    
    @IBAction func btnComentario(_ sender: Any) {
        
      let useruuid = Auth.auth().currentUser?.uid
            
            db.collection("comentario").addDocument(data: [
                "Comentario": txtcometario.text!,
                "useruuid": useruuid ?? ""
            ]) { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Order Express added successfully")
                }
            }
        
    }

}
